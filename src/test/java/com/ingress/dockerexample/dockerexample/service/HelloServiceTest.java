package com.ingress.dockerexample.dockerexample.service;

import com.ingress.dockerexample.dockerexample.model.entity.Counter;
import com.ingress.dockerexample.dockerexample.repository.HelloRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class HelloServiceTest {

    private static final Long DUMMY_ID = 1L;
    private static final Long DUMMY_COUNTER = 2L;

    private Counter counter;
    @InjectMocks
    HelloServiceImpl helloService;

    @Mock
    HelloRepository helloRepository;

    @Captor
    ArgumentCaptor<Long> argumentCaptor;

    void setUp() {
        counter = Counter
                .builder()
                .id(DUMMY_ID)
                .counter(DUMMY_COUNTER)
                .build();
    }

    @Test
    void givenIdNotInDbWhenGetCounterThenReturnNewCounter() {
        //Arrange
        when(helloRepository.findById(DUMMY_ID)).thenReturn(Optional.empty());

        //Action
        Counter counter = helloService.getCounter(DUMMY_ID);

        //Asserts
        verify(helloRepository, times(1)).findById(argumentCaptor.capture());
        Long captured = argumentCaptor.getValue();

        assertEquals(captured, DUMMY_ID);
        assertEquals(counter, new Counter(0L, 0L));
    }

}

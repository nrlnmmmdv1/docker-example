package com.ingress.dockerexample.dockerexample.web.rest;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingress.dockerexample.dockerexample.model.entity.Counter;
import com.ingress.dockerexample.dockerexample.service.HelloService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(HelloController.class)
public class HelloControllerTest {

    private static final Long DUMMY_ID = 1L;
    private static final Long DUMMY_COUNTER = 2L;
    private static final String BASE_URL = "/v1/hello";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    private HelloService helloService;

    private Counter counter;

    @BeforeEach
    void setUp() {
        counter = Counter
                .builder()
                .id(DUMMY_ID)
                .counter(DUMMY_COUNTER)
                .build();
    }

    @Test
    void givenInputWhenEqualToOneThenReturnCounterEqualTo1() throws Exception {
        //Arrange
        when(helloService.hello(1L)).thenReturn(counter);

        //Action
        ResultActions actions = mockMvc.perform(
                get(BASE_URL + "/" + DUMMY_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(counter)));

        //Assert
        verify(helloService, times(1)).hello(any(Long.class));
        actions.andExpect(status().isOk());
        actions.andExpect(content().string("{\"message\":\"Hello, the counter is 2\"}"));
    }

    @Test
    void givenInvalidIdWhenGetThenReturnBadRequestException() throws Exception {
        //Arrange
        String id = "1";

        //Action
        ResultActions actions = mockMvc.perform(
                get(BASE_URL + "/" + id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON));

        //Assert
        verify(helloService, times(1)).hello(any());
        actions.andExpect(status().isBadRequest());

    }
}

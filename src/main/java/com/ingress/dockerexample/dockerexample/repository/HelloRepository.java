package com.ingress.dockerexample.dockerexample.repository;

import com.ingress.dockerexample.dockerexample.model.entity.Counter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HelloRepository extends JpaRepository<Counter, Long> {
}

package com.ingress.dockerexample.dockerexample.service;

import com.ingress.dockerexample.dockerexample.model.entity.Counter;
import com.ingress.dockerexample.dockerexample.repository.HelloRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HelloServiceImpl implements HelloService {

    private final HelloRepository helloRepository;


    @Override
    public Counter hello(Long id) {
        Counter counter = getCounter(id);
        if(counter.getId() == 0)
            counter = saveCounter(id);
        increaseCounter(counter);
        return getCounter(id);
    }

    public Counter saveCounter(Long id) {
        return helloRepository.save(
                Counter.builder()
                        .id(id)
                        .counter(0L)
                        .build());
    }

    public void increaseCounter(Counter counter) {
        helloRepository.save(
                Counter.builder()
                        .id(counter.getId())
                        .counter(counter.getCounter() + 1)
                        .build());

    }

    public Counter getCounter(Long id) {
        return helloRepository
                .findById(id)
                .orElse(new Counter(0L, 0L));
//                .orElseThrow(CounterNotFoundException::new);
    }
}

package com.ingress.dockerexample.dockerexample.service;

import com.ingress.dockerexample.dockerexample.model.entity.Counter;
import org.springframework.stereotype.Service;

@Service
public interface HelloService {

    Counter hello(Long id);

}

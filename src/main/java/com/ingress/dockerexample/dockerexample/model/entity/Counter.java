package com.ingress.dockerexample.dockerexample.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "counter")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Counter {

    @Id
    private Long id;

    private Long counter;

}



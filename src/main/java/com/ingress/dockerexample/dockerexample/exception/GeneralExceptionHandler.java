package com.ingress.dockerexample.dockerexample.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

public class GeneralExceptionHandler extends ResponseEntityExceptionHandler {

    public GeneralExceptionHandler() {
        super();
    }

    @ExceptionHandler({ CounterNotFoundException.class })
     public ResponseEntity<Object> handleCounterNotFound(final RuntimeException ex, final WebRequest request) {
         final String bodyOfResponse = "Counter Not Found!";
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}

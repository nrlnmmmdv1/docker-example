package com.ingress.dockerexample.dockerexample.web.rest;

import com.ingress.dockerexample.dockerexample.model.entity.Counter;
import com.ingress.dockerexample.dockerexample.model.response.Result;
import com.ingress.dockerexample.dockerexample.service.HelloService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/hello")
@RequiredArgsConstructor
public class HelloController {

    private final HelloService helloService;

    @GetMapping("/{id}")
    public ResponseEntity<Result> hello(@PathVariable Long id) {
        Counter counter = helloService.hello(id);
        if (counter == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        return ResponseEntity
                .ok(Result
                        .builder()
                        .message("Hello, the counter is " + counter.getCounter())
                        .build());
    }
}

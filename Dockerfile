FROM alpine:3.14
RUN apk add --no-cache openjdk11
COPY build/libs/dockerExample-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java", "-jar", "/app/dockerExample-0.0.1-SNAPSHOT.jar"]
#CMD ["-jar", "/app/dockerExample-0.0.1-SNAPSHOT.jar"]